var states = 
{
	pause: 0,
	clear: 1,
	beforeClear: 2,
	title: 3,
	getOnStairs: 4,
	bubbling: 5,
	bubbled: 6,
	falling: 7,
	cave: 8, 
	garden: 9,
	touch: 10,
	end: 11,
};

var gameState;
var stateTimer;
var mouseTimer;
var frameCount;
var stateTimer;
var tintTimer;
var walkingCloudAlready;
var tmpFrameCount;

var grilBgImg;
var catBgImg;
var bubbledImg;
var frameImg;
var stairsImg;
var torchImg;
var touchTombFrameImg;
var farewellImg;

var cloud0Img;
var cloud1Img;
var cloud2Img;
var cloud3Img;
var cloud4Img;
var cloud5Img;
var cloud6Img;
var cloud7Img;

var bubbleImg;

var oto0Img;
var oto1Img;
var oto2Img;
var oto3Img;
var oto4Img;
var oto5Img;
var oto6Img;

var ora0Img;
var ora1Img;
var ora2Img;
var ora3Img;
var ora4Img;
var ora5Img;

var cliffImg;
var desertImg;
var endImg;
var hintImg;
var textBubbleImg;

var cat;
var girl;
var girlBg;
var catBg;
var frame;
var stairs;
var beforeClouds;
var behindClouds;
var bubbles;
var bubbledCat;
var bullets;
var hold;
var cliff;
var cave;
var torches;
var desert;
var endBg;
var hint;
var textBubble;
var oras;
var touchTombFrame;
var touchTomb;
var end;
var farewell;
var preBubble;

var touchTombAnimation;
var endAnimation;
var standingAnimation;
var walkingAnimation;
var slippingAnimation;
var fallingAnimation;
var violinAnimation;
var stairsAnimation;
var stairsStopAnimation;
var stairsNotMovingAnimation;
var stairsEndAnimation;
var otoHoldingAnimation;
var burningAnimation;
var caveLightAnimation;
var bubbleBurstAnimation;

var titleMusic;
var slippingMusic;
var fallingMusic;
var caveMusic;
var gardenMusic;
var endMusic;

var oto0Sound;
var oto1Sound;
var oto2Sound;
var oto3Sound;
var emptySound;
var preSound;

//nice pixel font
var pixelFont;
var sansFont;
var swingFont;
var geekFont;
var quinzeyFont;
var joinksFont;
var marigoldFont;
var stellaFont;
var uraniaFont;
var valpFont;
var cuteNoteFont;
var lefinaFont;

var tintSpeed = 32;

var catScale = 0.3;
var catMovingSpeed = 2;

var fallingTime = 1000;
var fallingSpeed0 = 16;
var fallingSpeed1 = 14;
var fallingSpeed2 = 12;
var fallingSpeed3 = 10;
var fallingSpeed4 = 8;
var fallingSpeed5 = 6;
var slippingSpeedX = 5;
var slippingSpeedY = 3.1;

var otoX = 800;
var otoY = 200;
var holdCatRelativePos = 65;
var bubbleFreq = 32;
var cloudFreq = 8;
var bubblingCloudFreq = 25;
var fallingToCaveTime = 150;

var oraFreq = 128;

var conPosX = 800;
var conPosY = 155;
var hintPosX = 405;
var hintPosY = 570;
var hintButtonPosX = 900;
var hintButtonPosY = 557;

function gameOver()
{
	gameState = states.over;
	updateSprites(false);
}

function gameBeforeOver()
{
	//stairs.changeImage("notMoving");
	gameState = states.beforeOver;
	stateTimer = 0;
}

function gameClear()
{
	gameState = states.clear;
	updateSprites(false);
}

function gameBeforeClear()
{
	gameState = states.beforeClear;
	stateTimer = 0;
}

function gamePause()
{
	gameState = states.pause;
	updateSprites(false);
}

function gameResume()
{
	gameState = states.bubbling;
	updateSprites(true);
}

function newGame()
{
	setup();

	updateSprites(true);
}

function customOverlap(inCamPosX, inCamPosY, outCamPosX, outCamPosY)
{
	var fakeX = inCamPosX - camera.position.x + width / 2;
	var fakeY = inCamPosY - camera.position.y + height / 2;
	var dx = fakeX - outCamPosX;
	var dy = fakeY - outCamPosY;

	return (dx * dx + dy * dy);	
}

function createRandomBubble()
{
	var h = random(20) * height / 20 + 32; 

	var bubble = createSprite(-10, h); 
	bubble.addImage(bubbleImg);
	bubble.addAnimation("burst", bubbleBurstAnimation);
	bubble.scale = catScale;

  	bubble.setCollider("circle", 0, 0, 230);
	bubble.setSpeed(random(3, 4), random(-60, 60));

	bubble.burst = false;

	bubbles.add(bubble);
}

function createRandomCloud()
{
	var w = cat.position.x + random(-600, 600);
	var tag = random(7) + 1;

	var cloud = createSprite(w, cat.position.y + 350); 
	switch (parseInt(tag)) 
	{
		case 0:
			//cloud.addImage(cloud0Img);
			break;
		case 1:
			cloud.addImage(cloud1Img);
			break;
		case 2:
			cloud.addImage(cloud2Img);
			break;
		case 3:
			cloud.addImage(cloud3Img);
			break;
		case 4:
			cloud.addImage(cloud4Img);
			break;
		case 5:
			cloud.addImage(cloud5Img);
			break;
		case 6:
			cloud.addImage(cloud6Img);
			break;
		case 7:
			cloud.addImage(cloud7Img);
			break;
		default:
			// statements_def
			break;
	}

	/*
	if (gameState == states.bubbling)
	{
		cloud.velocity.x = -1;
		cloud.velocity.y = -2.5;		
	}
	else
	if (gameState == states.falling)
	{
		cloud.velocity.x = 0;
		cloud.velocity.y = -12;
	}
	else
	{
		cloud.velocity.x = -0.05;
		cloud.position.y = random(0, 600);	
		cloud.position.x = 850;	
	}
	*/

	cloud.velocity.x = -0.05;
	if (parseInt(random(2)) == 0)
	{
		cloud.mirrorX(-1);
	}

	if (parseInt(random(3)) < 2)
	{
		behindClouds.add(cloud);
	}
	else
	{
		beforeClouds.add(cloud);
	}
}

function createRandomOra(h)
{
	var w = random(0, 750);
	var tag = random(6);

	var ora = createSprite(w, h); 
	switch (parseInt(tag)) 
	{
		case 0:
			ora.addImage(ora0Img);
			break;
		case 1:
			ora.addImage(ora1Img);
			break;
		case 2:
			ora.addImage(ora2Img);
			break;
		case 3:
			ora.addImage(ora3Img);
			break;
		case 4:
			ora.addImage(ora4Img);
			break;
		case 5:
			ora.addImage(ora5Img);
			break;
		default:
			// statements_def
			break;
	}

	/*
	if (gameState == states.bubbling)
	{
		cloud.velocity.x = -1;
		cloud.velocity.y = -2.5;		
	}
	else
	if (gameState == states.falling)
	{
		cloud.velocity.x = 0;
		cloud.velocity.y = -12;
	}
	else
	{
		cloud.velocity.x = -0.05;
		cloud.position.y = random(0, 600);	
		cloud.position.x = 850;	
	}
	*/

	ora.scale = random(5, 8) / 10;
	ora.velocity.y = - random(3, 5) / 10;
	if (parseInt(random(2)) == 0)
	{
		ora.mirrorX(-1);
	}

	oras.add(ora);
}

function createBullet(X, Y, flag)
{
	var w = otoX;
	var h = otoY;
	var x = X - w;
	var y = Y - h;
	var z = Math.sqrt(x*x + y*y);
	var angle = Math.round(Math.asin(Math.abs(y/z)) / Math.PI * 180); 

	if (x > 0)
	{
		if (y < 0)
		{
			angle = 180 - angle;
		}
		else
		{
			angle = 180 + angle;
		}
	}
	else
	{
		if (y > 0)
		{
			angle = 360 - angle;
		}
		else
		{
			angle = angle;
		}
	}

	var bullet = createSprite(w, h); 
  	bullet.setCollider("rectangle", 0, 0, 60, 60);
	bullet.setSpeed(random(2, 3) - 10, angle);

	var type = flag;
	if (parseInt(type) == -1)
	{
		type = random(6);
	}

	switch (parseInt(type)) 
	{
		case 0:
			bullet.addImage(oto0Img);
			bullet.sound = oto0Sound;
			break;
		case 1:
			bullet.addImage(oto1Img);
			bullet.sound = oto1Sound;
			break;
		case 2:
			bullet.addImage(oto2Img);
			bullet.sound = oto2Sound;
			break;
		case 3:
			bullet.addImage(oto3Img);
			bullet.sound = oto3Sound;
			break;
		case 4:
			bullet.addImage(oto4Img);
			bullet.sound = oto0Sound;
			break;
		case 5:
			bullet.addImage(oto5Img);
			bullet.sound = oto1Sound;
			break;
		default:
			// statements_def
			break;
	}

	bullet.sound.play();
	bullet.scale = 0.6;
	bullets.add(bullet);
}

//here we are going to load all the files and store them in the variables
//it's advisable to do this in the preload() function, which runs before the game starts
function preload() 
{
	girlBgImg = loadImage("assets/bg/bg0.png");
	catBgImg = loadImage("assets/bg/bg1.png");
	frameImg = loadImage("assets/bg/frame.png");
	bubbleImg = loadImage("assets/bubble/bubble.png");
	cliffImg = loadImage("assets/bg/cliff.png");
	bubbledImg = loadImage("assets/cat/bubbled0.png");
	torchImg = loadImage("assets/torch/torchEmpty.png");
	endImg = loadImage("assets/bg/bg3.png");
	hintImg = loadImage("assets/bg/hint.png");
	textBubbleImg = loadImage("assets/bg/textBubble.png");
	touchTombFrameImg = loadImage("assets/bg/frame0.png");
	farewellImg = loadImage("assets/bg/farewell.png");

	cloud0Img = loadImage("assets/bg/cloud0.png");
	cloud1Img = loadImage("assets/bg/cloud1.png");
	cloud2Img = loadImage("assets/bg/cloud2.png");
	cloud3Img = loadImage("assets/bg/cloud3.png");
	cloud4Img = loadImage("assets/bg/cloud4.png");
	cloud5Img = loadImage("assets/bg/cloud5.png");
	cloud6Img = loadImage("assets/bg/cloud6.png");
	cloud7Img = loadImage("assets/bg/cloud7.png");

	oto0Img = loadImage("assets/oto/oto0.png");
	oto1Img = loadImage("assets/oto/oto1.png");
	oto2Img = loadImage("assets/oto/oto2.png");
	oto3Img = loadImage("assets/oto/oto3.png");
	oto4Img = loadImage("assets/oto/oto4.png");
	oto5Img = loadImage("assets/oto/oto5.png");
	oto6Img = loadImage("assets/oto/oto6.png");

	ora0Img = loadImage("assets/bg/ora0.png");
	ora1Img = loadImage("assets/bg/ora1.png");
	ora2Img = loadImage("assets/bg/ora2.png");
	ora3Img = loadImage("assets/bg/ora3.png");
	ora4Img = loadImage("assets/bg/ora4.png");
	ora5Img = loadImage("assets/bg/ora5.png");

	standingAnimation = loadAnimation(
		"assets/cat/standing0.png", 
		"assets/cat/standing1.png",
		"assets/cat/standing2.png", 
		"assets/cat/standing3.png"
	);
	walkingAnimation = loadAnimation(
		"assets/cat/walking0.png", 
		"assets/cat/walking1.png",
		"assets/cat/walking2.png", 
		"assets/cat/walking3.png",
		"assets/cat/walking4.png", 
		"assets/cat/walking5.png",
		"assets/cat/walking6.png", 
		"assets/cat/walking7.png"
	);
	slippingAnimation = loadAnimation(
		"assets/cat/slipping0.png", 
		"assets/cat/slipping1.png",
		"assets/cat/slipping2.png", 
		"assets/cat/slipping3.png"
	);
	fallingAnimation = loadAnimation(
		"assets/cat/falling0.png", 
		"assets/cat/falling1.png",
		"assets/cat/falling2.png", 
		"assets/cat/falling3.png"
	);

	violinAnimation = loadAnimation(
		"assets/girl/violin0.png",
		"assets/girl/violin1.png",	
		"assets/girl/violin2.png",
		"assets/girl/violin3.png"	
	);

	stairsAnimation = loadAnimation(
		"assets/bg/stairs0.png", 
		"assets/bg/stairs1.png", 
		"assets/bg/stairs2.png" 
	);
	stairsStopAnimation = loadAnimation(
		"assets/bg/stairs3.png", 
		"assets/bg/stairs4.png", 
		"assets/bg/stairs5.png", 
		"assets/bg/stairs6.png", 
		"assets/bg/stairs7.png", 
		"assets/bg/stairs8.png", 
		"assets/bg/stairs9.png",
		"assets/bg/stairs10.png",
		"assets/bg/stairs11.png"
	);
	stairsStopAnimation.looping = false;
	stairsStopAnimation.frameDelay = 4;
	stairsNotMovingAnimation = loadAnimation(
		"assets/bg/stairs0.png", 
		"assets/bg/stairs0.png"
	);
	stairsEndAnimation = loadAnimation(
		"assets/bg/stairs11.png", 
		"assets/bg/stairs11.png"
	);
	
	otoHoldingAnimation = loadAnimation(
		"assets/oto/fallingEmpty.png",
		"assets/oto/falling0.png",
		"assets/oto/falling1.png",	
		"assets/oto/falling2.png",
		"assets/oto/falling3.png",
		"assets/oto/falling4.png",
		"assets/oto/falling5.png",	
	);
	otoHoldingAnimation.playing = false;

	burningAnimation = loadAnimation(
		"assets/torch/torch0.png",
		"assets/torch/torch1.png",
		"assets/torch/torch2.png",
		"assets/torch/torch3.png",	
	);
	burningAnimation.frameDelay = 6;
	caveLightAnimation = loadAnimation(
		"assets/bg/cave0.png",
		"assets/bg/cave1.png",
		"assets/bg/cave2.png",
		"assets/bg/cave3.png"
	);
	caveLightAnimation.playing = false;
	touchTombAnimation = loadAnimation(
		"assets/bg/touchTomb0.png",
		"assets/bg/touchTomb1.png",
		"assets/bg/touchTomb2.png",
		"assets/bg/touchTomb3.png",
		"assets/bg/touchTomb4.png",
		"assets/bg/touchTomb5.png"
	);
	touchTombAnimation.playing = false;
	endAnimation = loadAnimation(
		"assets/bg/wakening0.png",
		"assets/bg/wakening1.png",
		"assets/bg/wakening2.png",
		"assets/bg/wakening3.png",
		"assets/bg/wakening4.png",
		"assets/bg/end0.png",
		"assets/bg/end1.png",
		"assets/bg/end2.png",
		"assets/bg/end3.png"
	);
	endAnimation.playing = false;
	bubbleBurstAnimation = loadAnimation(
		"assets/bubble/bubble0.png",
		"assets/bubble/bubble1.png",
		"assets/bubble/bubble2.png"	
	);
	bubbleBurstAnimation.looping = false;
	bubbleBurstAnimation.frameDelay = 4;

	titleMusic = loadSound("assets/bgm/title.mp3");	
	slippingMusic = loadSound("assets/bgm/slipping.mp3");	
	fallingMusic = loadSound("assets/bgm/falling.mp3");	
	caveMusic = loadSound("assets/bgm/cave.mp3");	
	gardenMusic = loadSound("assets/bgm/garden.mp3");
	endMusic = loadSound("assets/bgm/end.mp3");
	/*
	titleMusic.loop();
	slippingMusic.loop();
	fallingMusic.loop();
	caveMusic.loop();
	gardenMusic.loop();
	*/

	oto0Sound = loadSound("assets/bgm/note0.mp3");	
	oto1Sound = loadSound("assets/bgm/note1.mp3");
	oto2Sound = loadSound("assets/bgm/note2.mp3");
	oto3Sound = loadSound("assets/bgm/note3.mp3");
	emptySound = loadSound("assets/bgm/note0.mp3");
	preSound = emptySound;

	/*
	pixelFont = loadFont('assets/fonts/arcadepix.ttf');	
	sansFont = loadFont('assets/fonts/Qarmic sans Abridged.ttf');	
	swingFont = loadFont('assets/fonts/SWINSBRG.ttf');
	geekFont = loadFont('assets/fonts/geektastic.ttf');	
	quinzeyFont = loadFont('assets/fonts/Quinzey_Medium.otf');	
	joinksFont = loadFont('assets/fonts/JOINKS.ttf');
	marigoldFont = loadFont('assets/fonts/Marigold-Regular.otf');
	stellaFont = loadFont('assets/fonts/Stella-Regular.otf');	
	*/
	uraniaFont = loadFont('assets/fonts/Urania Semi Serif.ttf');
	valpFont = loadFont('assets/fonts/Valpuesta.ttf');
	cuteNoteFont = loadFont('assets/fonts/Cute Notes.ttf');
	lefinaFont = loadFont('assets/fonts/Lefina.ttf');
}

//the setup() function runs once, after all the files are loaded in preload()
//generally this is used to set up the game world and all the objects
function setup() 
{
	// create the canvas with the specficed size.
	var canvas = createCanvas(1050, 600); 
	canvas.parent("container"); //position the canvas within the html, look in the index.html file for the div called "container"

	girlBg = createSprite(width / 2, height / 2); 
	girlBg.addImage(girlBgImg);

	catBg = createSprite(width / 2, height / 2); 
	catBg.addImage(catBgImg);
	//catBg.velocity.x = -0.2;
	//catBg.velocity.y = -0.7;

	frame = createSprite(width / 2, height / 2); 
	frame.addImage(frameImg);

	/*
	cat = createSprite(500, height / 2 + 55); 
	cat.rotation = -10;
	*/
	cat = createSprite(150, 410);
	cat.scale = catScale;
	cat.setCollider("circle", 0, 0, 150);

	standingAnimation.frameDelay = 16;
	walkingAnimation.frameDelay = 5;
	cat.addAnimation("standing", standingAnimation);
	cat.addAnimation("walking", walkingAnimation);
	cat.addAnimation("slipping", slippingAnimation);
	cat.addAnimation("falling", fallingAnimation);
	cat.changeAnimation("standing");

	girl = createSprite(width - 170, height / 2 + 40);
	girl.scale = 0.52;

	violinAnimation.frameDelay = 16;
	girl.addAnimation("violin", violinAnimation);
	girl.changeAnimation("violin");

	hint = createSprite(hintButtonPosX, hintButtonPosY);
	hint.addImage(hintImg);
	hint.scale = 0.73;
	hint.mouseActive = true;

	textBubble = createSprite(conPosX, conPosY);
	textBubble.addImage(textBubbleImg);

	behindClouds = new Group();
	beforeClouds = new Group();
	bubbles = new Group();
	bullets = new Group();
	torches = new Group();
	oras = new Group();

	gameState = states.title;
	mouseTimer = 0;
	stateTimer = 0;
	tintTimer = -256;
	walkingCloudAlready = false;

	titleMusic.loop();
}

function update()
{	
	/*
	if(keyDown("A")){
		cat.velocity.x = -1.5; //if I'm holding the left key, set the chicken's horizontal velocity to -3
		cat.mirrorX(-1); //flip the sprite to face left
	} 
	else 
	if (keyDown("D"))
	{
		cat.velocity.x = 1.5; //if I'm holding the right key, set the chicken's horizontal velocity to 3
		cat.mirrorX(1); //flip the sprite to face right
	}
	else {
		cat.velocity.x = 0; //if I'm not holding left or right, don't move in the horizontal axis
	}

	if (Math.abs(cat.velocity.x) > 0)
	{
		cat.changeAnimation("walking");
	}
	else
	{
		cat.changeAnimation("standing");
	}
	*/
	if (gameState == states.title)
	{
		if (cat.position.x == 150)
		{
			textFont(lefinaFont);
			textSize(80);
			textAlign(CENTER);
			text("Soundless", width / 2 - 120, 140 + 10 * Math.sin(frameCount * 0.01));
			text("Wakening", width / 2 - 120, 200 + 10 * Math.sin(frameCount * 0.01));

			if (hint.mouseIsOver)
			{
				textFont(cuteNoteFont);
				textSize(25);
				textAlign(CENTER);
				text("Click Simon to start", hintPosX, hintPosY);
			}

		  	if (mouseWentDown(LEFT))
			{
				createBullet(mouseX, mouseY, -1);
			}

			for (var i = 0; i < bullets.length; ++i)
			{
				var bullet = bullets[i];
				if (bullet.overlap(cat))
				{
					/*
					preSound.stop();
					preSound = bullet.sound;
					bullet.sound.play();
					*/
					bullets.remove(bullet);

					cat.velocity.x = catMovingSpeed;
					cat.changeAnimation("walking");
				}
			}
		}
		else
		if (cat.position.x > 150)
		{
			drawSprite(textBubble);
			textFont(uraniaFont);
			textSize(25);
			textAlign(CENTER);
			text("Come here, Simon.", conPosX, conPosY);

			if (cat.position.x == 500)
			{
				stairs = createSprite(width / 2, height / 2);
				stairs.addAnimation("moving", stairsAnimation);
				stairs.addAnimation("stop", stairsStopAnimation);
				stairs.addAnimation("notMoving", stairsNotMovingAnimation);
				stairs.addAnimation("end", stairsEndAnimation);
				//stairs.changeAnimation("moving");

				cat.changeAnimation("slipping");
				cat.position.x = -50;
				cat.position.y = -55; 
				cat.rotation = -10;
				cat.setCollider("circle", 0, 0, 150);
				cat.velocity.x = slippingSpeedX;
				cat.velocity.y = slippingSpeedY;

				catBg.remove();

				titleMusic.stop();
				slippingMusic.loop();
				gameState = states.bubbling;
				/*
				stairs = createSprite(width / 2, height / 2);
				stairs.addAnimation("moving", stairsAnimation);
				stairs.addAnimation("stop", stairsStopAnimation);
				stairs.addAnimation("notMoving", stairsNotMovingAnimation);
				stairs.addAnimation("end", stairsEndAnimation);
				stairs.changeAnimation("moving");
								
				cat.changeAnimation("slipping");
				cat.position.x = 500;
				cat.position.y = height / 2 + 55; 
				cat.rotation = -10;
				cat.setCollider("circle", 0, 0, 150);
				cat.velocity.x = 0;

				catBg.remove();

				gameState = states.bubbling;
				*/
			}
		}
	}
	else
	if (gameState == states.getOnStairs)
	{

	}
	else
	// new bubbles
	if (gameState == states.bubbling)
	{
		if (cat.position.x >= width / 2 || cat.position.y >= height / 2)
		{
			camera.position.x = cat.position.x;
			camera.position.y = cat.position.y;

			/*
			// change state
			if (keyDown("ESC"))
			{
				gamePause();
			}
			*/
			
			//textFont(uraniaFont);
			if (hint.mouseIsOver)
			{
				textFont(cuteNoteFont);
				textSize(25);
				textAlign(CENTER);
				text("Click to burst the bubbles", hintPosX, hintPosY);
			}

			++stateTimer;

			if (stateTimer >= fallingTime + 60)
			{
				// change to falling
				//stairs.remove();
				stairs.changeAnimation("end");

				stairs.velocity.y = -fallingSpeed0;

				cat.changeAnimation("falling");
				//cat.position.x = width / 2 - 200;
				//cat.position.y = height / 2;
				cat.setCollider("circle", 0, 0, 220);
				cat.rotation = 0;
				cat.velocity.x = 0;
				cat.velocity.y = fallingSpeed0;

				/*
				for (var i = 0; i < beforeClouds.length; ++i)
				{
					var cloud = beforeClouds[i];
					cloud.velocity.x = 0;
					cloud.velocity.y = -12;
				}
				for (var i = 0; i < behindClouds.length; ++i)
				{
					var cloud = behindClouds[i];
					cloud.velocity.x = 0;
					cloud.velocity.y = -12;
				}
				*/

				hold = createSprite(cat.position.x, cat.position.y + holdCatRelativePos);
				hold.velocity.y = fallingSpeed0;
				hold.scale = 0.5;
				hold.addAnimation("holding", otoHoldingAnimation);
				hold.changeAnimation("holding");

				stateTimer = 0;
				mouseTimer = 0;

				slippingMusic.stop();
				fallingMusic.loop();
				gameState = states.falling;
			}
			else
			if (stateTimer >= fallingTime + 40)
			{
				stairs.changeAnimation("stop");			
			}
			if (stateTimer >= fallingTime)
			{
				if (walkingCloudAlready == false)
				{
					var walkingCloud = createSprite(cat.position.x + 300, cat.position.y + 290);
					walkingCloud.addImage(cloud0Img);
					walkingCloud.velocity.x = 0;
					walkingCloud.velocity.y = 0;
					//walkingCloud.velocity.y = -7;
					beforeClouds.add(walkingCloud);
					walkingCloudAlready = true;

					cat.setCollider("circle", 20, 0, 0);
					
					for (var i = 0; i < bubbles.length; ++i)
					{
						var bubble = bubbles[i];
						bubble.changeAnimation("burst");
					}
				}		
			}
			else
			if (stateTimer <= fallingTime - 40)
			{
				// create scenes
				if (parseInt(random(bubbleFreq)) == 0)
				{
					createRandomBubble();
				}
			}

			if (parseInt(random(bubblingCloudFreq)) == 0)
			{
				createRandomCloud();
			}


			// bounce on edges
		  	for (var i = 0; i < bubbles.length; i++) 
		  	{
		 		var bubble = bubbles[i];

		 		if (bubble.burst)
		 		{
			 		if (bubble.animation.getFrame() == 2)
			 		{
			 			bubbles.remove(bubble);
			 		}
		 		}
		 		else
		 		{
		 		/*
		  		if(bubble.position.x < 10) 
		  		{
		    		bubble.position.x = 10;
		    		bubble.velocity.x = abs(bubble.velocity.x);
		 		}
		  
		  		if(bubble.position.x > width - 250) 
		  		{
			    	bubbles.remove(bubble);
		    	}
		  
		 		if(bubble.position.y < 10) 
		 		{
		    		bubble.position.y = 10;
		    		bubble.velocity.y = abs(bubble.velocity.y);
		  		}
		  
		  		if(bubble.position.y > height - 10) 
		  		{
				    bubble.position.y = height - 9;
				    bubble.velocity.y = -abs(bubble.velocity.y);
		   		} 
				*/

					// bullet
			   		for (var j = 0; j < bullets.length; j++)
			   		{
			   			var bullet = bullets[j];
			   			if (bullet.overlap(bubble) && bubble.position.x >= 80)
			   			{
			   				bubble.changeAnimation("burst");
			   				bubble.burst = true;
			   				/*
							preSound.stop();
							preSound = bullet.sound;
			   				bullet.sound.play();
			   				*/
			   				bullets.remove(bullet);
			   			}
			   		}

			   		// bubbled
			   		if (customOverlap(cat.position.x, cat.position.y, bubble.position.x, bubble.position.y) < 8000)
			   		//if (bubble.overlap(cat))
			   		{
			   			bubbledCat = createSprite(520, 300);
			   			bubbledCat.addImage(bubbledImg);
			   			bubbledCat.scale = catScale;
			   			bubbledCat.setCollider("circle", 0, 10, 70);
			   			preBubble = bubble;

			   			cat.visible = false;
			   			cat.velocity.x = 0;
			   			cat.velocity.y = 0;

			   			bubble.visible = false;
			   			bubble.velocity.x = 0;
			   			bubble.setCollider("circle", 0, 0, 0);

			   			stairs.animation.stop();
			   			/*
						for (var i = 0; i < beforeClouds.length; ++i)
						{
							var cloud = beforeClouds[i];
							cloud.velocity.x = -0.05;
							cloud.velocity.y = 0;
						}
						for (var i = 0; i < behindClouds.length; ++i)
						{
							var cloud = behindClouds[i];
							cloud.velocity.x = -0.05;
							cloud.velocity.y = 0;
						}
						*/
						gameState = states.bubbled;
			   			//bubble.changeAnimation("over");
			   			//bubble.velocity.y = -6;
			   			//cat.remove();
			   			//gameBeforeOver();
			   		}
		   		}
		  	}

		  	//bubbles.bounce(bubbles);

		  	// bullets
		  	if (mouseWentDown(LEFT))
			{
				createBullet(mouseX, mouseY, -1);
			}
		}
	}
	else
	if (gameState == states.bubbled)
	{
		if (hint.mouseIsOver)
		{
			textFont(cuteNoteFont);
			textSize(25);
			textAlign(CENTER);
			text("Click to burst the bubbles", hintPosX, hintPosY);
		}

		if (parseInt(random(1000)) == 0)
		{
			createRandomCloud();
		}

		bubbledCat.position.y = 350 + 30 * Math.sin(frameCount * 0.02);

		for (var i = 0; i < bullets.length; ++i)
		{
			var bullet = bullets[i];
			if (bullet.overlap(bubbledCat))
			{
				cat.visible = true;
	   			cat.velocity.x = slippingSpeedX;
	   			cat.velocity.y = slippingSpeedY;

	   			/*
				preSound.stop();
				preSound = bullet.sound;
				bullet.sound.play();
				*/
				bubbledCat.remove();
				preBubble.visible = true;
				preBubble.changeAnimation("burst");
				preBubble.burst = true;
				bullets.remove(bullet);

				stairs.animation.play();

				/*
				for (var i = 0; i < beforeClouds.length; ++i)
				{
					var cloud = beforeClouds[i];
					cloud.velocity.x = -1;
					cloud.velocity.y = -2.5;
				}
				for (var i = 0; i < behindClouds.length; ++i)
				{
					var cloud = behindClouds[i];
					cloud.velocity.x = -1;
					cloud.velocity.y = -2.5;
				}
				*/

				gameState = states.bubbling;
				break;
			}

		}

	  	// bullets
	  	if (mouseWentDown(LEFT))
		{
			createBullet(mouseX, mouseY, -1);
		}
	}
	if (gameState == states.falling)
	{
		if (hint.mouseIsOver)
		{
			textFont(cuteNoteFont);
			textSize(25);
			textAlign(CENTER);
			text("Hold the mouse to catch Simon", hintPosX, hintPosY);
		}

		if (parseInt(random(cloudFreq)) == 0)
		{
			createRandomCloud();
		}

		camera.position.x = cat.position.x + 100 - 100 * Math.cos(0.02 * stateTimer);
		camera.position.y = cat.position.y;

		++stateTimer;
		if (parseInt(stateTimer) >= 100)
		{
			if (mouseWentUp(LEFT))
			{
				mouseTimer++;
				createBullet(mouseX, mouseY, hold.animation.getFrame());


				/*
				if (parseInt(mouseTimer) == 120)
				{
					createBullet(mouseX, mouseY, 0);	
				}
				else
				if (parseInt(mouseTimer) == 100)
				{
					createBullet(mouseX, mouseY, 1);	
				}
				else
				if (parseInt(mouseTimer) == 80)
				{
					createBullet(mouseX, mouseY, 6);	
				}
				else
				if (parseInt(mouseTimer) == 65)
				{
					createBullet(mouseX, mouseY, 4);	
				}
				else
				if (parseInt(mouseTimer) == 40)
				{
					createBullet(mouseX, mouseY, 5);	
				}
				else
				if (parseInt(mouseTimer) == 20)
				{
					createBullet(mouseX, mouseY, 2);	
				}
				*/
			}

			for (var i = 0; i < bullets.length; ++i)
			{
				var bullet = bullets[i];
				if (customOverlap(cat.position.x, cat.position.y, bullet.position.x, bullet.position.y) < 6000)
				{
					hold.animation.nextFrame();

					/*
					preSound.stop();
					preSound = bullet.sound;
					bullet.sound.play();
					*/
					bullets.remove(bullet);
				}
			}

			var frameNum = hold.animation.getFrame();
			switch(parseInt(frameNum))
			{
				case 0:
					cat.velocity.y = fallingSpeed0;
					hold.velocity.y = fallingSpeed0;
					break;
				case 1:
					cat.velocity.y = fallingSpeed1;
					hold.velocity.y = fallingSpeed1;
					break;
				case 2:
					cat.velocity.y = fallingSpeed2;
					hold.velocity.y = fallingSpeed2;
					break;
				case 3:
					cat.velocity.y = fallingSpeed3;
					hold.velocity.y = fallingSpeed3;
					break;
				case 4:
					cat.velocity.y = fallingSpeed4;
					hold.velocity.y = fallingSpeed4;
					break;
				case 5:
					cat.velocity.y = fallingSpeed5;
					hold.velocity.y = fallingSpeed5;
					break;
				case 6:
					fallingMusic.stop();
					caveMusic.loop();

					tmpFrameCount = stateTimer;
					gameState = states.cave;
					stateTimer = 0;	
					break;						
			}
		}
		/*
		++stateTimer;

		if (parseInt(random(27)) == 0)
		{
			createRandomCloud();
		}

		if (parseInt(stateTimer) >= 100)
		{
			if (mouseDown(LEFT))
			{
				mouseTimer++;
				if (parseInt(mouseTimer) == 120)
				{
					createBullet(cat.position.x, cat.position.y + holdCatRelativePos, 0);	
				}
				else
				if (parseInt(mouseTimer) == 100)
				{
					createBullet(cat.position.x, cat.position.y + holdCatRelativePos, 1);	
				}
				else
				if (parseInt(mouseTimer) == 80)
				{
					createBullet(cat.position.x, cat.position.y + holdCatRelativePos, 6);	
				}
				else
				if (parseInt(mouseTimer) == 65)
				{
					createBullet(cat.position.x, cat.position.y + holdCatRelativePos, 4);	
				}
				else
				if (parseInt(mouseTimer) == 40)
				{
					createBullet(cat.position.x, cat.position.y + holdCatRelativePos, 5);	
				}
				else
				if (parseInt(mouseTimer) == 20)
				{
					createBullet(hold.position.x, hold.position.y + 20, 2);	
				}
			}

			for (var i = 0; i < bullets.length; ++i)
			{
				var bullet = bullets[i];
				if (bullet.overlap(cat))
				{
					hold.animation.nextFrame();

					bullets.remove(bullet);
				}
			}

			// change to cave
			if (hold.animation.getFrame() == 6)
			{
				//cat.velocity.y = 10;
				//hold.velocity.y = 10;	
				gameState = states.cave;
				stateTimer = 0;			
			}
		}
		*/
	}
	else
	if (gameState == states.cave)
	{
		++stateTimer;

		// on the ground
		if (parseInt(stateTimer) > fallingToCaveTime + 50)
		{
			if (cat.position.y >= 390)
			{
				// bullets
				if (mouseWentUp(LEFT))
				{
					createBullet(mouseX, mouseY, -1);
				}	

				// just land & oto fly away
				if (cat.velocity.y != 0)
				{
					cat.velocity.y = 0;
					cat.position.y = 410;			
					cat.setCollider("circle", 0, 0, 150);
					cat.changeAnimation("standing");

					hold.velocity.y = -5;
					hold.velocity.x = 1 + 2 * Math.sin(stateTimer * 0.1);
				}
				// on ground & standing
				else
				if (cave.velocity.x == 0 && cave.position.x > width - 50)
				{
					if (hint.mouseIsOver)
					{
						textFont(cuteNoteFont);
						textSize(25);
						textAlign(CENTER);
						text("Click Simon to guide him", hintPosX, hintPosY);
					}

					// start walking by clicking
					for (var i = 0; i < bullets.length; ++i)
					{
						var bullet = bullets[i];
						if (bullet.overlap(cat))
						{
							cave.velocity.x = -catMovingSpeed;
							cat.changeAnimation("walking");

							/*
							preSound.stop();
							preSound = bullet.sound;							
							bullet.sound.play();
							*/
							bullets.remove(bullet);
						}
					}
				}
				else
				{
					// walking
					if (hint.mouseIsOver)
					{
						textFont(cuteNoteFont);
						textSize(25);
						textAlign(CENTER);
						text("Click to light torches", hintPosX, hintPosY);
					}	

					for (var i = 0; i < bullets.length; ++i)
					{
						var bullet = bullets[i];
						for (var j = 0; j < torches.length; ++j)
						{
							var torch = torches[j];
							if (bullet.overlap(torch))
							{
								torch.changeAnimation("burning");
								torch.lit = true;
								torch.setCollider("circle", 0, 0, 0);	

								cave.animation.nextFrame();

								/*
								preSound.stop();
								preSound = bullet.sound;
								bullet.sound.play();
								*/
								bullets.remove(bullet);
							}
						}
					}	

					if (parseInt(cave.position.x) > 550)
					{
						drawSprite(textBubble);
						textFont(uraniaFont);
						textSize(25);
						textAlign(CENTER);
						text("This way.", conPosX, conPosY);
					}

					// out of cave
					if (parseInt(cave.position.x) < -250)
					{
						cave.velocity.x = 0;
						cat.velocity.x = catMovingSpeed;		

						for (var i = 0; i < torches.length; ++i)
						{
							var torch = torches[i];
							torch.velocity.x = 0;
						}

						if (cat.position.x >= 200)
						{
							if (torches[2].lit != true)
							{
								cat.velocity.x = 0;	
								cat.changeAnimation("standing");		
							}	
							else
							{
								cat.velocity.x = catMovingSpeed;
								cat.changeAnimation("walking");			
							}	
						}

						if (cat.position.x >= 750)
						{
							/*
							desert = createSprite(width / 2, height / 2);
							desert.addImage(desertImg);
							desert.velocity.x = -0.2;

							cat.position.x = 150;
							cat.velocity.x = 0;
							cat.scale = 0.2;

							torches.removeSprites();

							gameState = states.desert;
							*/

							endBg = createSprite(width / 2, height / 2);
							endBg.addImage(endImg);

							torches.removeSprites();

							cat.position.x = -30;
							cat.velocity.x = 2;
							cat.scale = catScale;

							caveMusic.stop();
							gardenMusic.loop();

							for (var j = 0; j < 5; ++j)
							{
								createRandomOra(random(100, height-100));
							}
							gameState = states.garden;
						}		
					}
					else
					// careful of float speed
					if (parseInt(cave.position.x) == 950 || parseInt(cave.position.x) == 150 ||
						parseInt(cave.position.x) == 550)
					{
						var torch = createSprite(800, 200);
						torch.addImage(torchImg);
						torch.addAnimation("burning", burningAnimation);
						torch.velocity.x = -catMovingSpeed;
						torch.lit = false;

						if (parseInt(cave.position.x) == 950)
						{
							torch.setCollider("circle", 0, 0, 50);
						}
						else
						{
							torch.setCollider("circle", 0, 0, 0);
						}

						torches.add(torch);
					}
					else
					if (parseInt(cave.position.x) == 500)
					{
						if (torches[0].lit != true)
						{
							cave.velocity.x = 0;
							cat.velocity.x = 0;	
							cat.changeAnimation("standing");	

							for (var i = 0; i < torches.length; ++i)
							{
								var torch = torches[i];
								torch.velocity.x = 0;
							}	
						}
						else
						{
							cave.velocity.x = -catMovingSpeed;
							cat.changeAnimation("walking");		

							for (var i = 0; i < torches.length; ++i)
							{
								var torch = torches[i];
								torch.velocity.x = -catMovingSpeed;
							}		

							torches[1].setCollider("circle", 0, 0, 40);	
						}			
					}	
					else
					if (parseInt(cave.position.x) == 100)
					{
						if (torches[1].lit != true)
						{
							cave.velocity.x = 0;
							cat.velocity.x = 0;	
							cat.changeAnimation("standing");	

							for (var i = 0; i < torches.length; ++i)
							{
								var torch = torches[i];
								torch.velocity.x = 0;
							}	
						}	
						else
						{
							cave.velocity.x = -catMovingSpeed;
							cat.changeAnimation("walking");		

							for (var i = 0; i < torches.length; ++i)
							{
								var torch = torches[i];
								torch.velocity.x = -catMovingSpeed;
							}		

							torches[2].setCollider("circle", 0, 0, 40);	
						}	
					}			
				}
			}
		}
		else
		// falling into cave
		if (parseInt(stateTimer) == fallingToCaveTime + 50)
		{
			cat.position.y = -50;
			cat.position.x = 150;
			hold.position.x = cat.position.x;
			hold.position.y = cat.position.y + holdCatRelativePos;

			cave = createSprite(width - 30, height / 2 - 30);
			cave.addAnimation("light", caveLightAnimation);
			cave.changeAnimation("light");

			camera.position.x = width / 2;
			camera.position.y = height / 2;
		}
		else
		if (parseInt(stateTimer) > fallingToCaveTime)
		{
			drawSprite(textBubble);
			textFont(uraniaFont);
			textSize(25);
			textAlign(CENTER);
			text("You'll be fine.", conPosX, conPosY);

			cat.velocity.y = fallingSpeed5;
			hold.velocity.y = fallingSpeed5;	

			camera.position.x = cat.position.x + 100 - 100 * Math.cos(0.02 * tmpFrameCount);
			//camera.position.y = cat.position.y;
		}
		else
		{
			drawSprite(textBubble);
			textFont(uraniaFont);
			textSize(25);
			textAlign(CENTER);
			text("You'll be fine.", conPosX, conPosY);

			camera.position.x = cat.position.x + 100 - 100 * Math.cos(0.02 * tmpFrameCount);
			camera.position.y = cat.position.y;
		}
	}
	else
	if (gameState == states.garden)
	{
		if (mouseWentDown(LEFT))
		{
			createBullet(mouseX, mouseY, -1);
		}

		if (cat.position.x >= 450)
		{
			cat.velocity.x = 0;
			cat.changeAnimation("standing");

			if (hint.mouseIsOver)
			{
				textFont(cuteNoteFont);
				textSize(25);
				textAlign(CENTER);
				text("Click Simon", hintPosX, hintPosY);
			}
			
			for (var i = 0; i < bullets.length; ++i)
			{
				var bullet = bullets[i];
				if (bullet.overlap(cat))
				{
					bullets.remove(bullet);
					oras.removeSprites();

					touchTombFrame = createSprite(width / 2, height / 2); 
					touchTombFrame.addImage(touchTombFrameImg);

					touchTomb = createSprite(width / 2, height / 2); 
					touchTomb.addAnimation("touch", touchTombAnimation);
					touchTomb.changeAnimation("touch");

					gardenMusic.stop();
					endMusic.play();

					gameState = states.touch;
					stateTimer = 0;
				}
			}
		}

		if (parseInt(random(oraFreq)) == 0)
		{
			createRandomOra(height);
		}
	}
	else
	if (gameState == states.touch)
	{
		++stateTimer;

		if (touchTomb.animation.getFrame() <= 1)
		{
			if (touchTomb.position.y <= height / 2 - 20)
			{
				touchTomb.velocity.y = 0;
			}
			else
			{
				touchTomb.velocity.y = -0.03;		
			}
		}
		else 
		{
			if (touchTomb.position.y >= height / 2 + 15)
			{
				touchTomb.velocity.y = 0;
			}
			else
			{
				touchTomb.velocity.y = 0.03;		
			}		
		}

		if (tintTimer > -255)
		{
			tintTimer = tintTimer - tintSpeed;
		}

		var frameTime;
		var frame = parseInt(touchTomb.animation.getFrame());
		if (frame <= 1)
		{
			frameTime = 80;
		}
		else
		if (frame <= 5)
		{
			frameTime = 60;
		}
		/*
		else
		if (frame == 6)
		{
			frameTime = 80;
		}
		else
		{
			frameTime = 100;
		}
		*/

		if (stateTimer == -1)
		{
			if (frame == 4)
			{
				stateTimer = 0;
				touchTomb.animation.nextFrame();
			}
			else
			{
				end = createSprite(width / 2, height / 2); 
				end.addAnimation("ending", endAnimation);
				touchTomb.remove();
				touchTombFrame.remove();
				//farewell.visible = false;

				stateTimer = 0;
				gameState = states.end;	
			}
		}

		if (frame == 3 || frame == 4 || frame == 6)
		{
			if (stateTimer >= frameTime)
			{
				textFont(cuteNoteFont);
				textSize(25);
				textAlign(CENTER);
				text("Click to continue", 550, 555);	

				if (mouseWentDown(LEFT))
				{
					/*
					if (frame == 6)
					{
						farewell = createSprite(width / 2, height / 2); 
						farewell.addImage(farewellImg);						
					}
					*/

					if (frame == 6 || frame == 4)
					{
						tintTimer = 256;
						stateTimer = -9;	
					}
					else
					{
						touchTomb.animation.nextFrame();
						stateTimer = 0;
					}	
				}							
			}
		}
		else
		{
			if (stateTimer >= frameTime)
			{
				if (frame == 5)
				{
					tintTimer = 256;
					stateTimer = -9;
				}
				else
				{
					touchTomb.animation.nextFrame();
					stateTimer = 0;
				}
			}
		}
	}
	else
	if (gameState == states.end)
	{
		++stateTimer;
		if (tintTimer > -255)
		{
			tintTimer = tintTimer - tintSpeed;
		}

		var frame = parseInt(end.animation.getFrame());
		if (frame <= 1)
		{
			if (stateTimer >= 30)
			{
				end.animation.nextFrame();
				stateTimer = 0;
			}
		}
		else
		if (frame == 2)
		{
			if (stateTimer >= 100)
			{
				end.animation.nextFrame();
				stateTimer = 0;
			}			
		}
		else
		if (frame == 3)
		{
			if (stateTimer >= 80)
			{
				textFont(cuteNoteFont);
				textSize(25);
				textAlign(CENTER);
				text("Click to continue", 550, 555);	

				if (mouseWentDown(LEFT))	
				{
					farewell = createSprite(width / 2, height / 2); 
					farewell.addImage(farewellImg);	

					end.animation.nextFrame();
					stateTimer = 0;
				}			
			}
		}
		else
		if (frame == 4)
		{
			if (stateTimer >= 100)
			{
				textFont(cuteNoteFont);
				textSize(25);
				textAlign(CENTER);
				text("Click to continue", 550, 555);

				if (mouseWentDown(LEFT))
				{
					tintTimer = 256;
					stateTimer = -65;
					tintSpeed = 4;
				}			
			}

			if (stateTimer == -1)
			{
				end.animation.nextFrame();
				stateTimer = 0;
			}

		}
		else
		if (frame == 5)
		{
			if (stateTimer == 122)
			{
				tintTimer = 256;
				tintSpeed = 32;					
			}

			if (stateTimer == 130)
			{
				//farewell.remove();

				end.animation.nextFrame();
				stateTimer = 0;
			}
		}
		else
		{
			if (stateTimer == 62 && frame != 8)
			{
				tintTimer = 256;
				tintSpeed = 32;
			}

			if (stateTimer >= 70)
			{
				if (frame == 8)
				{
					farewell.remove();

					gameState = states.clear;
					stateTimer = 0;
				}
				else
				{
					end.animation.nextFrame();
					stateTimer = 0;
				}
			}
		}
	}
	else
	if (gameState == states.pause)
	{
		textSize(30);
		textAlign(CENTER);
		text("Game Paused", width / 2 - 120, height / 2);
		textSize(13);
		text("Press ENTER to continue.", width / 2 - 120, height / 2 + 30);
		text("Press R to restart.", width / 2 - 120, height / 2 + 50);

		preSecond = second();

		if (keyDown("ENTER"))
		{
			gameResume();		
		}
		else
		if (keyDown("R"))
		{
			newGame();		
		}
	}
	else
	if (gameState == states.over)
	{
		textSize(20);
		textAlign(CENTER);
		text("Game Over", width / 2 - 120, height / 2);
		textSize(13);
		text("Press R to restart.", width / 2 - 120, height / 2 + 50);

		if (keyDown("R"))
		{
			newGame();		
		}
	}
	else
	if (gameState == states.clear)
	{
		++stateTimer;

		if (stateTimer >= 200)
		{
			textFont(cuteNoteFont);
			textSize(25);
			textAlign(CENTER);
			text("Click to restart", 250, 575);	

			if (mouseWentDown(LEFT))
			{
				endMusic.stop();
				newGame();
			}
		}

		textFont(cuteNoteFont);
		textSize(25);
		textAlign(CENTER);
		text("The End", 900, 575);	
	}
	else
	if (parseInt(stateTimer) < 100)
	{
		stateTimer++;
		if (parseInt(stateTimer) >= 100)
		{
			if (gameState == states.beforeClear)
			{
				gameClear();
			}
			else
			if (gameState == states.beforeOver)
			{
				gameOver();
			}
		}
	}


	/*
	//same again for vertical directions
	if(keyDown("UP")){
		chicken.velocity.y = -3;
	} 
	else if (keyDown("DOWN")){
		chicken.velocity.y = 3;
	}
	else {
		chicken.velocity.y = 0;
	}
	*/
	/*
	//now that we've set the velocity, we can talk about what should happen when the bird is moving or standing still
	if ((Math.abs(chicken.velocity.x) > 0 ) || (Math.abs(chicken.velocity.y) > 0)){
		chicken.changeAnimation("walking"); //if we're moving, play the walking animation
	}
	else {
		chicken.changeAnimation("standing"); //play the standing still animation

		//if we're standing still, we might want the chicken to be able to lay an egg!
		//first check we're not sitting on an egg!

		//this goes through all the eggs, and returns true if any egg is overlapping the chicken
		if (eggs.overlap(chicken)==false){ 

			if (keyDown("SPACE")){ 

				chicken.changeAnimation("sitting"); //sit the chicken down
				chicken.eggTimer = chicken.eggTimer + 1; //we're going to count how many frames we've been sitting down
				if (chicken.eggTimer > 30){
					//just as an example, let's print a message to the console. These are useful for debugging!
					console.log("laying an egg!")

					//we've been sitting for 30 frames. make an egg!
					//the egg consists of two sprites, for the top and bottom half
					var newEggTop = createSprite(chicken.position.x, chicken.position.y);
					newEggTop.addImage(eggTopImg);
					
					//I need to keep track of how long the egg has been around so we can know when it should hatch.
					//To do that I'm going to add a variable to the sprite called hatchTimer
					//random(x,y) returns a random decimal number between x and y, but we want whole numbers here so
					//I'm using the Math.round function to round the number to an integer.
					newEggTop.hatchTimer = Math.round( random(60,140) ); 
					newEggTop.hatchDirection = -1; //which way the egg will move when it hatches
					eggs.add(newEggTop); //add the egg section to the group of eggs

					var newEggBottom = createSprite(chicken.position.x, chicken.position.y);
					newEggBottom.addImage(eggBottomImg);
					newEggBottom.hatchTimer = newEggTop.hatchTimer;
					newEggBottom.hatchDirection = 1;
					eggs.add(newEggBottom);
					
					laySound.play();

					chicken.eggTimer = 0; //reset the mouseTimer so we can't lay another egg too soon
				}

			}
			else {
				chicken.eggTimer = 0; //we're not sitting down anymore... reset the mouseTimer
			}
		}


		
	}


	//2. PROCESSING EGG BEHAVIOR

	//handle egg hatching!
	for (var i=0; i<eggs.length; i++){ //we're iterating over all the eggs in the array
		var thisEgg = eggs[i]; //get a reference to the egg at index i
		thisEgg.hatchTimer = thisEgg.hatchTimer - 1; //count down the egg's mouseTimer until it hatches by 1
		
		if ((thisEgg.hatchTimer < 20)&&(thisEgg.hatchTimer > 0)){
			//the egg is almost ready to hatch! let's make it rattle a bit by moving it left and right
			//the modulo operator '%' gives you the remainder after dividing by that number, so:
			//5 % 2 = 1, 4 % 2 = 0, etc. It's a useful way to find out if a number is even or odd.
			//this line returns true for one frame and then false for one frame in a loop
			if (thisEgg.hatchTimer % 2 == 0){
				thisEgg.position.x = thisEgg.position.x + 2;
			}
			else {
				thisEgg.position.x = thisEgg.position.x - 2;
			}
		}	

		//when the mouseTimer gets to zero, we'll play a sound
		if (thisEgg.hatchTimer ==0){
			hatchSound.play();
		}

		if (thisEgg.hatchTimer <= 0){
			//hatch the egg! This will just mean moving the two halves of the egg away from each other
			thisEgg.position.y += 10*thisEgg.hatchDirection;

			//NOTE: Something else should happen here, right? You can't have a game where
			//eggs hatch and nothing comes out.
		}
		if (thisEgg.hatchTimer <= -10){
			//now that we've pushed the two halves apart, we can destroy them.
			eggs.remove(thisEgg);
		}
		
	}
	*/
}

//The draw() function is called once a frame to draw all the things in the game.
//The order we draw them matters - newer things are drawn on top of older things
//So we start with the background
function draw() {
	//the background() function fills the background with a color. The color is given in web-style hex format
	//it's just three two-digit numbers stuck together, for the red, green and blue components.
	//the only catch it's they're hexadecimal numbers, which go 0123456789ABCDEF instead of 0123456789
	//so white would be 0xffffff, red would be 0xff0000, and blue would be 0x0000ff.
	background(0xffffff);	
	fill(50, 50, 50);

	//here we call our custom update function to process input and movement
	if (gameState == states.title)
	{
		camera.on();
		drawSprite(catBg);
		drawSprite(cat);
	}
	else
	if (gameState == states.getOnStairs || gameState == states.bubbling || gameState == states.bubbled
		|| gameState == states.falling)
	{
		camera.on();
		drawSprites(behindClouds);

		camera.off();
		drawSprite(stairs);
		drawSprite(bubbledCat);
		camera.on();

		drawSprite(cat);
		drawSprite(hold);
		drawSprites(beforeClouds);	

		camera.off();
		drawSprites(bubbles);
	}
	else
	if (gameState == states.cave)
	{
		camera.on();	
		drawSprite(cave);	
		drawSprites(behindClouds);
		drawSprite(cat);
		drawSprite(hold);
		drawSprites(torches);
		drawSprites(beforeClouds);	
	}
	else
	if (gameState == states.garden)
	{
		camera.on();
		drawSprite(endBg);
		drawSprite(cat);		

		camera.off();
		drawSprites(oras);		
	}

	if(gameState == states.touch)
	{
		tint(255, 255, 255, Math.abs(tintTimer));
		drawSprite(touchTomb);

		tint(255, 255, 255, 255);
		drawSprite(touchTombFrame);
	}
	else
	if (gameState == states.end || gameState == states.clear)
	{
		tint(255, 255, 255, Math.abs(tintTimer));
		drawSprite(end);

		tint(255, 255, 255, 255);
		drawSprite(farewell);		
	}
	else
	{
		camera.off();

		drawSprite(girlBg);
		drawSprite(girl);

		drawSprite(frame);
		drawSprite(hint);
		drawSprites(bullets);
	}

	update(); 
}


