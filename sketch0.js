//here we are going to load all the files and store them in the variables
//it's advisable to do this in the preload() function, which runs before the game starts
var states = 
{
	pause: 0,
	clear: 1,
	beforeClear: 2,
	title: 3,
	getOnStairs: 4,
	bubbling: 5,
	bubbled: 6,
	falling: 7,
	cave: 8, 
	garden: 9,
	touch: 10,
	end: 11,
};

var gameState = states.touch;

var stateTimer;
var tintTimer;
var tintSpeed = 32;

var touchTombFrame;

var touchTomb;
var end;
var farewell;

var touchTombFrameImg;

var farewellImg;

var touchTombAnimation;
var endAnimation;

var cameraSpeed = 0.005;

var cuteNoteFont;

var endMusic;

function preload() 
{
	touchTombFrameImg = loadImage("assets/bg/frame0.png");

	farewellImg = loadImage("assets/bg/farewell.png");

	touchTombAnimation = loadAnimation(
		"assets/bg/touchTomb0.png",
		"assets/bg/touchTomb1.png",
		"assets/bg/touchTomb2.png",
		"assets/bg/touchTomb3.png",
		"assets/bg/touchTomb4.png",
		"assets/bg/touchTomb5.png"
	);
	touchTombAnimation.playing = false;
	endAnimation = loadAnimation(
		"assets/bg/wakening0.png",
		"assets/bg/wakening1.png",
		"assets/bg/wakening2.png",
		"assets/bg/wakening3.png",
		"assets/bg/wakening4.png",
		"assets/bg/end0.png",
		"assets/bg/end1.png",
		"assets/bg/end2.png",
		"assets/bg/end3.png"
	);
	endAnimation.playing = false;

	cuteNoteFont = loadFont('assets/fonts/Cute Notes.ttf');

	endMusic = loadSound("assets/bgm/end.mp3");
}

//the setup() function runs once, after all the files are loaded in preload()
//generally this is used to set up the game world and all the objects
function setup() 
{
	// create the canvas with the specficed size.
	var canvas = createCanvas(1050, 600); 
	canvas.parent("container"); //position the canvas within the html, look in the index.html file for the div called "container"

	touchTombFrame = createSprite(width / 2, height / 2); 
	touchTombFrame.addImage(touchTombFrameImg);

	touchTomb = createSprite(width / 2, height / 2); 
	touchTomb.addAnimation("touch", touchTombAnimation);
	touchTomb.changeAnimation("touch");

	endMusic.play();

	stateTimer = 0;
	tintTimer = -256;
}

function update()
{	
	if (gameState == states.touch)
	{
		++stateTimer;

		if (touchTomb.animation.getFrame() <= 1)
		{
			if (touchTomb.position.y <= height / 2 - 20)
			{
				touchTomb.velocity.y = 0;
			}
			else
			{
				touchTomb.velocity.y = -0.03;		
			}
		}
		else 
		{
			if (touchTomb.position.y >= height / 2 + 15)
			{
				touchTomb.velocity.y = 0;
			}
			else
			{
				touchTomb.velocity.y = 0.03;		
			}		
		}

		if (tintTimer > -255)
		{
			tintTimer = tintTimer - tintSpeed;
		}

		var frameTime;
		var frame = parseInt(touchTomb.animation.getFrame());
		if (frame <= 1)
		{
			frameTime = 100;
		}
		else
		if (frame <= 5)
		{
			frameTime = 100;
		}
		else
		if (frame == 6)
		{
			frameTime = 80;
		}
		else
		{
			frameTime = 100;
		}

		if (stateTimer == -1)
		{
			if (frame == 4)
			{
				stateTimer = 0;
				touchTomb.animation.nextFrame();
			}
			else
			{
				end = createSprite(width / 2, height / 2); 
				end.addAnimation("ending", endAnimation);
				touchTomb.remove();
				touchTombFrame.remove();
				//farewell.visible = false;

				stateTimer = 0;
				gameState = states.end;	
			}
		}

		if (frame == 3 || frame == 4 || frame == 6)
		{
			if (stateTimer >= frameTime)
			{
				textFont(cuteNoteFont);
				textSize(25);
				textAlign(CENTER);
				text("Click to continue", 550, 555);	

				if (mouseWentDown(LEFT))
				{
					/*
					if (frame == 6)
					{
						farewell = createSprite(width / 2, height / 2); 
						farewell.addImage(farewellImg);						
					}
					*/

					if (frame == 6 || frame == 4)
					{
						tintTimer = 256;
						stateTimer = -9;	
					}
					else
					{
						touchTomb.animation.nextFrame();
						stateTimer = 0;
					}	
				}							
			}
		}
		else
		{
			if (stateTimer >= frameTime)
			{
				if (frame == 5)
				{
					tintTimer = 256;
					stateTimer = -9;
				}
				else
				{
					touchTomb.animation.nextFrame();
					stateTimer = 0;
				}
			}
		}
	}
	else
	if (gameState == states.end)
	{
		++stateTimer;
		if (tintTimer > -255)
		{
			tintTimer = tintTimer - tintSpeed;
		}

		var frame = parseInt(end.animation.getFrame());
		if (frame <= 1)
		{
			if (stateTimer >= 30)
			{
				end.animation.nextFrame();
				stateTimer = 0;
			}
		}
		else
		if (frame == 2)
		{
			if (stateTimer >= 100)
			{
				end.animation.nextFrame();
				stateTimer = 0;
			}			
		}
		else
		if (frame == 3)
		{
			if (stateTimer >= 80)
			{
				textFont(cuteNoteFont);
				textSize(25);
				textAlign(CENTER);
				text("Click to continue", 550, 555);	

				if (mouseWentDown(LEFT))	
				{
					farewell = createSprite(width / 2, height / 2); 
					farewell.addImage(farewellImg);	

					end.animation.nextFrame();
					stateTimer = 0;
				}			
			}
		}
		else
		if (frame == 4)
		{
			if (stateTimer >= 100)
			{
				textFont(cuteNoteFont);
				textSize(25);
				textAlign(CENTER);
				text("Click to continue", 550, 555);

				if (mouseWentDown(LEFT))
				{
					tintTimer = 256;
					stateTimer = -65;
					tintSpeed = 4;
				}			
			}

			if (stateTimer == -1)
			{
				end.animation.nextFrame();
				stateTimer = 0;
			}

		}
		else
		if (frame == 5)
		{
			if (stateTimer == 152)
			{
				tintTimer = 256;
				tintSpeed = 32;					
			}

			if (stateTimer == 160)
			{
				//farewell.remove();

				end.animation.nextFrame();
				stateTimer = 0;
			}
		}
		else
		{
			if (stateTimer == 92 && frame != 8)
			{
				tintTimer = 256;
				tintSpeed = 32;
			}

			if (stateTimer >= 100)
			{
				if (frame == 8)
				{
					farewell.remove();

					textFont(cuteNoteFont);
					textSize(25);
					textAlign(CENTER);
					text("The End", 900, 575);	
				}
				else
				{
					end.animation.nextFrame();
					stateTimer = 0;
				}
			}
		}
	}
}

//The draw() function is called once a frame to draw all the things in the game.
//The order we draw them matters - newer things are drawn on top of older things
//So we start with the background
function draw() {
	//the background() function fills the background with a color. The color is given in web-style hex format
	//it's just three two-digit numbers stuck together, for the red, green and blue components.
	//the only catch it's they're hexadecimal numbers, which go 0123456789ABCDEF instead of 0123456789
	//so white would be 0xffffff, red would be 0xff0000, and blue would be 0x0000ff.
	background(0xffffff);	
	fill(50, 50, 50);

	//here we call our custom update function to process input and movement

	tint(255, 255, 255, Math.abs(tintTimer));
	drawSprite(touchTomb);
	drawSprite(end);

	tint(255, 255, 255, 255);
	drawSprite(touchTombFrame);
	drawSprite(farewell);

	update(); 
}


